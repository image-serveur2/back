from unittest import TestCase

from src.loader.image import Image


class ImageTest(TestCase):

    def test_nom(self):
        image = Image(nom="test")
        self.assertEqual('test', image.nom, "Nom doit être égal à 'test'")
